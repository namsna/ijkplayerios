//
//  ViewController.swift
//  ijkplayerIOS
//
//  Created by Nam Tran on 3/20/20.
//  Copyright © 2020 Nam Tran. All rights reserved.
//

import UIKit
import IJKMediaFramework

class ViewController: UIViewController {

    var player: IJKFFMoviePlayerController!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let options = IJKFFOptions.byDefault()
        //options?.setFormatOptionValue("tcp", forKey: "rtsp_transport")
        let url = URL(string: "rtsp://118.69.76.212:2554/Stream?url=rtsp://10.2.11.51&id=test&pw=Camera123")

        guard let player = IJKFFMoviePlayerController(contentURL: url, with: options) else {
            print("Create RTSP Player failed")
            return
        }

        let autoresize = UIView.AutoresizingMask.flexibleWidth.rawValue |
            UIView.AutoresizingMask.flexibleHeight.rawValue
        player.view.autoresizingMask = UIView.AutoresizingMask(rawValue: autoresize)

        player.view.frame = self.view.bounds
        player.scalingMode = IJKMPMovieScalingMode.aspectFit
        player.shouldAutoplay = true
        self.view.autoresizesSubviews = true
        self.view.addSubview(player.view)
        self.player = player
        self.player.prepareToPlay()
    }


}

